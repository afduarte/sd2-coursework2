﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HolidaySystem;


namespace UnitTesting
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void CreateValidBooking()
        {
            // Using the String constructor to avoid messing with the store singleton class
            // Try creating Customer with valid data
            Customer cust = new Customer(0,"Antero Duarte", "Flat 70 Dundee Street");
            Assert.AreEqual(cust.Name,"Antero Duarte","The name is not the same, constructor failed");
            Assert.AreEqual(cust.Address, "Flat 70 Dundee Street", "The address is not the same, constructor failed");
            Assert.AreEqual(cust.CustomerId, 0, "The name id not the same, constructor failed");

            // Using the String constructor to avoid messing with the store singleton class
            // Try creating Booking with valid data
            Booking book = new Booking(0, cust, "20-11-2016", "21-11-2016");
            Assert.AreEqual(book.Customer, cust, "Not the same customer reference, booking constructor failed");
            Assert.AreEqual(book.Nights, 1, "The amount of nights is not right!");
            Assert.AreEqual(book.GetCost(), 0, "The cost is wrong for no guests, 1 night, should be 0");
            // Try creating guest with valid data
            Guest g = new Guest("0000000000", "John Smith", 33);
            Assert.AreEqual(g.Name,"John Smith","The guest doen't have the right name, constructor failed");
            // Try adding guest to booking
            book.AddGuest(g);
            // Check if the cost is updated correctly
            Assert.AreEqual(book.GetCost(),50,"The cost is not the expected cost, should be 50");
            // Check if adding an extra results in the right cost
            book.AddExtra(new Breakfast(book,""));
            Assert.AreEqual(book.GetCost(), 55, "The cost is not the expected cost, should be 55");
            // Check that cost doubles when adding a new guest
            book.AddGuest(new Guest("1111111111","Jane Smith", 35));
            Assert.AreEqual(book.GetCost(), 110, "The cost is not the expected cost, should be 110");
        }
        [TestMethod]
        [ExpectedException(typeof(Exception), "The Name can not be empty")]
        public void TesCustomerValidation_Name()
        {
            // Using the String constructor to avoid messing with the store singleton class
            // Try creating Customer with empty name and  valid address
            Customer cust = new Customer(0, "", "some address");
        }
        [TestMethod]
        [ExpectedException(typeof(Exception), "The address can not be empty")]
        public void TestCustomerValidation_Address()
        {
            // Using the String constructor to avoid messing with the store singleton class
            // Try creating Customer with valid name and empty address
            Customer cust = new Customer(0, "Some Name", "");
        }
        [TestMethod]
        [ExpectedException(typeof(Exception), "A booking must span at least one night")]
        public void TestBookingValidation_Equal_Dates()
        {
            // Using the String constructor to avoid messing with the store singleton class
            // Try creating Customer with valid name and address
            Customer cust = new Customer(0, "Some Name", "Some Address");
            // Using the String constructor to avoid messing with the store singleton class
            // Try creating Booking with arrival date equal to departure date
            Booking book = new Booking(0, cust, "21-11-2016", "20-11-2016");
        }
        [TestMethod]
        [ExpectedException(typeof(Exception), "A booking must not have a departure date chronologically after it's arrival date")]
        public void TestBookingValidation_Switched_Dates()
        {
            // Using the String constructor to avoid messing with the store singleton class
            // Try creating Customer with valid name and address
            Customer cust = new Customer(0, "Some Name", "Some Address");
            // Using the String constructor to avoid messing with the store singleton class
            // Try creating Booking with arrival date after departure date
            Booking book = new Booking(0, cust, "21-11-2016", "20-11-2016");
        }
        [TestMethod]
        [ExpectedException(typeof(Exception), "The guest you are trying to add is already registered for this booking")]
        public void TestBookingValidation_Duplicated_Guest()
        {
            // Using the String constructor to avoid messing with the store singleton class
            // Try creating Customer with valid name and address
            Customer cust = new Customer(0, "Some Name", "Some Address");
            // Using the String constructor to avoid messing with the store singleton class
            // Try creating Booking with arrival date after departure date
            Booking book = new Booking(0, cust, "21-11-2016", "20-11-2016");
            Guest g = new Guest("0000000000", "John Smith", 33);
            book.AddGuest(g);
            book.AddGuest(g);
        }
        [TestMethod]
        [ExpectedException(typeof(Exception), "This booking already has the maximum amount of guests(4) associated with it")]
        public void TestBookingValidation_Guest_Limit()
        {
            // Using the String constructor to avoid messing with the store singleton class
            // Try creating Customer with valid name and address
            Customer cust = new Customer(0, "Some Name", "Some Address");
            // Using the String constructor to avoid messing with the store singleton class
            // Try creating Booking with arrival date after departure date
            Booking book = new Booking(0, cust, "21-11-2016", "20-11-2016");
            Guest g = new Guest("0000000000", "John Smith", 33);
            int i = 0;
            while (true)
            {
                book.AddGuest(new Guest("000000000"+i, "John Smith", 33));
                i++;
            }
        }
    }
}
