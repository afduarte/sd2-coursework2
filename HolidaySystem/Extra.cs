﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HolidaySystem
{
    public abstract class Extra : CSVWritable
    {
        /*
         * Antero Ferreira Duarte - 40211946
         * Extra Class(abstract) - Implements CSVWritable(interface)
         * Abstract class that creates a contract for every sub-class to have a GetCost() 
         * and a ToCSV method (through CSVWritable)
         * Last Modified: 05-12-2016
         */
        public abstract String ToCSV();
        public abstract double GetCost();
    }
}
