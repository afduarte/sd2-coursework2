﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidaySystem
{
    public class Guest : Person, CSVWritable
    {
        /*
         * Antero Ferreira Duarte - 40211946
         * Guest Class - Implements CSVWritable(interface) - Inherits from Person
         * Creates a new Guest object validating input values
         * Provides access to its private attributes through {get; set;} methods
         * Last Modified: 22-11-2016
         */
        // Keep a private variable to store the age
        private int age;
        // Public accessor methods for the age, perform the required validation
        public int Age
        {
            get
            {
                return this.age;
            }
            set
            {
                if (value < 0 || value > 101)
                {
                    throw new FormatException("Age must be a number between 0 and 101");
                }
                this.age = value;
            }
        }
        // Keep a private variable to store the passport
        private String passport;
        // Public accessor methods for the Passport, perform the required validation
        public String Passport {
            get
            {
                return this.passport;
            }
            set
            {
                if (String.IsNullOrWhiteSpace(value) || String.IsNullOrEmpty(value) || value.Length > 10)
                {
                    throw new FormatException("Invalid Passport nr: Must be between 1 and 10 characters");
                }

                this.passport = value;
            } 
        }
        // Guest constructor that takes in all parameters and calls the base class (Person) with the name
        public Guest(String passport, String name, int age)
            : base(name)
        {
            this.Age = age;
            this.Passport = passport;
        }
        // Implementation of the ToCSV method
        public String ToCSV()
        {
            String sep = CSVUtil.Separator.ToString();
            // Returns the values needed to store separated by a tab
            string[] props = new string[] {
                CSVUtil.Format(this.Passport.ToString()),
                CSVUtil.Format(this.Name),
                CSVUtil.Format(this.Age.ToString())
            };
            return String.Join(sep, props);
        }
    }
}
