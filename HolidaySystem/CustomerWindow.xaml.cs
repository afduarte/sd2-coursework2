﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HolidaySystem
{
    public partial class CustomerWindow : Window
    {
        /*
        * Antero Ferreira Duarte - 40211946
        * MainWindow Class
        * Handles Handles creating and changing customers
        * Last Modified: 05-12-2016
        */
        private Customer cust;
        public CustomerWindow()
        {
            InitializeComponent();
            this.cust = null;
        }
        public CustomerWindow(Customer c)
        {
            InitializeComponent();
            this.cust = c;
            txt_Name.Text = this.cust.Name;
            txt_Address.Text = this.cust.Address;
        }
        private void btn_Add_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.cust == null)
                {
                    this.cust = new Customer(txt_Name.Text, txt_Address.Text);
                    StoreFacade.AddCustomer(this.cust);
                }
                else
                {
                    this.cust.Name = txt_Name.Text;
                    this.cust.Address = txt_Address.Text;
                }
                
                this.Close();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
    }
}
