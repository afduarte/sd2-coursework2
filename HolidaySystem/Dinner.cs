﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidaySystem
{
    class Dinner : Extra
    {
        /*
         * Antero Ferreira Duarte - 40211946
         * Dinner Class - Inherits From Extra(abstract) - Relates to Booking (belongs to)
         * Creates a new dinner extra validating input values, 
         * overrides GetCost() from Extra and ToCSV() originally from CSVWritable (through Extra)
         * Last Modified: 05-12-2016
         */
        // Keep a private reference to the booking object
        private Booking booking;
        // Keep a private reference to the description
        private String description;
        // Public accessor for the description
        public String Description
        {
            get
            {
                return this.description;
            }
            set
            {
                this.description = value;
            }
        }
        // Constructor that sets the booking and description
        public Dinner(Booking b,String description)
        {
            this.booking = b;
            this.Description = description;

        }
        // Override of the GetCost method in the parent class
        public override double GetCost()
        {
            // Return the cost per day (add 1 to the Nights to get the number of different dates, not just the number of days in between)
            // This assumes the guests have dinner on the day they check in
            return  15 * this.booking.Guests.Count * (this.booking.Nights + 1);
        }
        // Override of the ToCSV method in the parent class
        public override string ToCSV()
        {
            String sep = CSVUtil.Separator.ToString();
            // Returns the values needed to store separated by a tab
            string[] props = new string[] {
                CSVUtil.Format(this.booking.BookingId.ToString()),
                CSVUtil.Format(this.description),
                CSVUtil.Format(this.GetType().Name)
            };
            return String.Join(sep, props);
        }
    }
}
