﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidaySystem
{
    public class Store
    {
        /*
         * Antero Ferreira Duarte - 40211946
         * Store Class - Relates to [1..1] Store (as per the specification for the Singleton Pattern) 
         * and [1..1] CSVUtil (to handle the persistance)
         * Keeps a list at runtime of all the bookings, customers and guests.
         * It is also responsible for persisting the data on MainWindow [closing event]
         * And restoring the data before MainWindow [InitializeComponent()]
         * Provides method for adding, removing, and getting every type of entity stored
         * Is also responsible for generating auto incremented ID numbers
         * Last Modified: 09-12-2016
         */
        // Keep a reference to the CSV Singleton
        private CSVUtil csv = CSVUtil.Instance;
        // Implement the class as a singleton
        private static Store instance;
        // Public accessor to the store instance
        public static Store Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Store();
                }
                return instance;
            }
            set { }
        }

        // Dictionaries to store bookings, customers and guests.
        private Dictionary<int, Booking> bookings = new Dictionary<int,Booking>();
        private Dictionary<int, Customer> customers = new Dictionary<int,Customer>();
        private Dictionary<string, Guest> guests = new Dictionary<string,Guest>();
        // Block the constructor so no objects of this class can be created
        private Store() { }
        // Method to load the customers from the file, adds a customer for each line in the CSV
        public void LoadCustomers()
        {
            foreach (String[] line in csv.ReadFromFile(typeof(HolidaySystem.Customer)))
            {
                this.AddCustomer(new Customer(Int32.Parse(line[0]), line[1], line[2]));
            }
        }
        // Method to persist the list of customers
        public void SaveCustomers()
        {
            foreach (Customer cust in customers.Values)
            {
                csv.AppendToFile(cust);
            }
        }
        // Method to load the bookings from the file, adds a booking for each line in the CSV
        // Also binds the right guest to the booking and skips it if the customer was deleted
        public void LoadBookings()
        {
            foreach (String[] line in csv.ReadFromFile(typeof(HolidaySystem.Booking))) 
            {
                Customer cust = this.customers[Int32.Parse(line[1])];
                if (cust == null)
                {
                    // Log the event of a non existing customer and skip this booking
                    Console.WriteLine("The customer "+line[1]+" is not registered in the system");
                    continue;
                }
                Booking book = new Booking(Int32.Parse(line[0]), cust, line[2], line[3]);

                foreach(String passport in line[4].Split(',')){
                    // Handle bookings without registered guests
                    if (String.IsNullOrEmpty(passport) || String.IsNullOrWhiteSpace(passport)) continue;
                    Guest g = this.guests[passport];
                    if (g == null)
                    {
                        // Log the event of a non existing guest and skip this guest
                        Console.WriteLine("The guest with passport number: " + passport + " is not registered in the system");
                        continue;
                    }
                    else
                    {
                        book.AddGuest(g);
                    }
                }
                this.AddBooking(book);

            }
        }
        // Method to persist the list of bookings
        public void SaveBookings()
        {
            foreach (Booking book in bookings.Values)
            {
                foreach (Extra e in book.Extras.Values)
                {
                    csv.AppendToFile(e);
                }
                csv.AppendToFile(book);
            }
        }
        // Method to load the bookings from the file, adds a booking for each line in the CSV
        public void LoadGuests()
        {
            foreach (String[] line in csv.ReadFromFile(typeof(HolidaySystem.Guest)))
            {
                this.AddGuest(new Guest(line[0], line[1], Int32.Parse(line[2])));
            }
        }
        // Method to persist the list of guests
        public void SaveGuests()
        {
            foreach (Guest g in guests.Values)
            {
                csv.AppendToFile(g);
            }
        }
        // Method that generates an ID for a new customer
        public int GenCustomerId()
        {
            if (this.customers.Count == 0) return 1;

            return this.customers.Keys.Max() + 1;
        }
        // Method to add a new customer to the store or update an existing one
        public void AddCustomer(Customer cust)
        {
            this.customers[cust.CustomerId] = cust;
        }
        // Method that deletes an existing customer from the store
        public void DeleteCustomer(Customer cust)
        {
            if (this.GetBookings().Where(book => 
            {
                return book.Customer == cust;
                
            }).Any(book => 
            {
                return book.DepartureDate > DateTime.Today;
            }))
            {
                throw new Exception("Can not delete customers with active or future bookings");
            }
            else
            {
                this.customers.Remove(cust.CustomerId);
            }
        }
        // Method that returns a customer when provided with an ID
        public Customer GetCustomer(int id)
        {
            return this.customers[id];
        }
        // Returns the list of customers
        public List<Customer> GetCustomers()
        {
            return this.customers.Values.ToList();
        }
        // Method that generates an ID for a new booking
        public int GenBookingId()
        {
            if (this.bookings.Count == 0) return 1;

            return this.bookings.Keys.Max() + 1;
        }
        // Method to add a new booking to the store or update an existing one
        public void AddBooking(Booking book)
        {
            this.bookings[book.BookingId] = book;
        }
        // Method that deletes an existing customer from the store
        public void DeleteBooking(Booking book)
        {
            this.bookings.Remove(book.BookingId);
        }
        // Method that returns a booking when provided with an ID
        public Booking GetBooking(int id)
        {
            return this.bookings[id];
        }
        // Returns the list of bookings
        public List<Booking> GetBookings()
        {
            return this.bookings.Values.ToList();
        }
        // Method to add a new guest to the store or update an existing one
        public void AddGuest(Guest guest)
        {
            this.guests[guest.Passport] = guest;
        }
        // Method that returns a customer when provided with a passport
        public Guest GetGuest(String id)
        {
            return this.guests[id];
        }
        // Returns the list of guests
        public List<Guest> GetGuests()
        {
            return this.guests.Values.ToList();
        }
        // Method to load the extras from the file, adds the extra to the right booking for each line in the CSV
        public void LoadExtras()
        {
            foreach (String[] line in csv.ReadFromFile(typeof(HolidaySystem.CarHire)))
            {
                Booking b = this.GetBooking(Int32.Parse(line[0]));
                if (b == null)
                {
                    Console.WriteLine("Failed to get booking " + line[0] + " to add a CarHire");
                    continue;
                }
                b.AddExtra(new CarHire(b, line[1], line[2], line[3]));
            }
            foreach (String[] line in csv.ReadFromFile(typeof(HolidaySystem.Breakfast)))
            {
                Booking b = this.GetBooking(Int32.Parse(line[0]));
                if (b == null)
                {
                    Console.WriteLine("Failed to get booking " + line[0] + " to add " + line[3]);
                    continue;
                }
                if (line[2] == typeof(HolidaySystem.Breakfast).Name)
                {
                    b.AddExtra(new Breakfast(b, line[1]));
                }
                else if (line[2] == typeof(HolidaySystem.Dinner).Name)
                {
                    b.AddExtra(new Dinner(b, line[1]));
                }
            }
        }
        // Method that clears the files
        public void ClearFiles()
        {
            this.csv.ClearFiles();
        }
    }
}
