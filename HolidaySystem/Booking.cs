﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidaySystem
{   
    public class Booking : CSVWritable
    {
        /*
         * Antero Ferreira Duarte - 40211946
         * Booking Class - Implements CSVWritable(interface) - Relates to [1..1] Customer and [0..MAX_GUESTS] Guest
         * Creates a new booking object validating input values, 
         * implements GetCost(), even though it doesn't have to formally (not enforced by OO principles), just for consistency
         * Provides access to its private attributes through {get; set;} methods
         * Last Modified: 05-12-2016
         */
        // Defines the maximum number of guests at any time for a booking. Declared as a static readonly 
        // so it is easily changeable if the business rules ever change
        private static readonly int MAX_GUESTS = 4;
        // Keep a list of the guests associated with this booking, initialize the list with the maximum number of guests
        // So it doesn't have to be resized
        private List<Guest> guests = new List<Guest>(MAX_GUESTS);
        // Method to add a guest to the booking, takes a Guest object and throws exceptions if it's a duplicated guest
        // or the booking has the maximum amount of guests associated with it already.
        public void AddGuest(Guest guest)
        {
            if (guests.IndexOf(guest) >= 0)
            {
                throw new Exception("The guest you are trying to add is already registered for this booking");
            }
            if (guests.Count < MAX_GUESTS)
            {
                guests.Add(guest);
            }
            else
            {
                throw new Exception("This booking already has the maximum amount of guests(" + MAX_GUESTS + ") associated with it");
            }
            
        }
        // Removes a guest from the booking, throws an exception if the guest is not found in the booking
        public void RemoveGuest(Guest g)
        {
            // remove returns false if
            if (!guests.Remove(g))
            {
                throw new Exception("Something went wrong, the guest(" + g.Name + ") was not found in this booking");
            }
        }
        // Public accessor for the list of guests, block the setter so guests have to be added individually
        public List<Guest> Guests 
        {
            get
            {
                return this.guests;
            }
            set { } 
        }
        // Dictionary that keeps track of all the extras associated with this booking
        private Dictionary<String,Extra> extras = new Dictionary<String,Extra>();
        // Public accessor for the dictionary, blocks the setter
        public Dictionary<String, Extra> Extras
        {
            get
            {
                return this.extras;
            }
            set { }
        }
        // Method to add/update an extra, basically uses the type of extra as a key in the dictionary
        public void AddExtra(Extra e)
        {
            this.extras[e.GetType().Name] = e;
        }
        // Method to remove an extra, takes in the key (type of extra)
        public bool RemoveExtra(String extra)
        {
            return this.extras.Remove(extra);
        }
        // Keep a private reference to the customer object associated with this booking
        private Customer cust;
        // Public accessor methods for the customer object
        public Customer Customer { 
            get 
            {
                return this.cust;
            }
            set
            {
                this.cust = value;
            }
        }
        // Keep a DateTime object for the arrival date
        private DateTime arrivalDate;
        // Public accessor methods for the arrival date object, includes validation
        public DateTime ArrivalDate 
        {
            get
            {
                return this.arrivalDate;
            }
            set
            {
                if (value == DateTime.MinValue)
                {
                    throw new Exception("Invalid date: Arrival");
                }
                this.arrivalDate = value;
            } 
        }
        // Keep a DateTime object for the departure date
        private DateTime departureDate;
        // Public accessor methods for the departure date object, includes validation
        public DateTime DepartureDate 
        { 
            get
            {
                return this.departureDate;
            }
            set
            {
                if (value == DateTime.MinValue)
                {
                    throw new Exception("Invalid date: Departure");
                }
                this.departureDate = value;
            }
        }
        // Keep a unique booking ID assigned by the store
        private int bookingId;
        // Public accessor for getting, block the setter since the ID is assigned when the object is constructed
        public int BookingId
        {
            get
            {
                return this.bookingId;
            }
            // do nothing with the set method
            set{}
        }
        // Apply constructor chaining for code reusability
        // Basic (id,cust) constructor only sets the costumer and gets the id as an Integer
        // Keep it private because it is only used inside the class
        private Booking(int id, Customer cust)
        {
            this.bookingId = id;
            this.Customer = cust;

        }
        // More complex constructor validates the DateTime objects passed in and chains into the upper constructor
        // Keep it private because it is only used inside the class
        private Booking(int id, Customer cust, DateTime arrivalDate, DateTime departureDate) :  this(id, cust)
        {
            if(arrivalDate.Date == departureDate.Date)
            {
                throw new Exception("A booking must span at least one night");
            }

            if(arrivalDate > departureDate)
            {
                throw new Exception("A booking must not have a departure date chronologically after it's arrival date");
            }
            this.ArrivalDate = arrivalDate;
            this.DepartureDate = departureDate;

        }
        // First public constructor used by the GUI, passes in a Customer object and two DateTime objects.
        // Delegates the generation ob the booking ID to the store and chains it into the upper constructor
        public Booking(Customer cust, DateTime arrivalDate, DateTime departureDate) 
            : this(StoreFacade.GenerateBookingId(), cust, arrivalDate, departureDate) { }
        // Second public constructor
        // The constructor used by the csv reader, must take in an id parameter that is read from the file
        // Takes in the dates as strings and converts them when chaining
        public Booking(int id, Customer cust, String arrivalDate, String departureDate)
            : this(id, cust, Convert.ToDateTime(arrivalDate), Convert.ToDateTime(departureDate)) { }
        // Public Nights property returns the number of nights in a booking, no set method
        public int Nights
        {
            get 
            { 
                return (this.departureDate - this.arrivalDate).Days; 
            }
        }
        // Public method that returns the arrival date as a string
        // Used in a binding in the GUI and the ToCSV() Method
        public String ArrivalDateStr
        {
            get
            {
                String dateFormat = "dd-MM-yyyy";
                return this.ArrivalDate.ToString(dateFormat);
            }
        }
        // Public method that returns the departure date as a string
        // Used in a binding in the GUI and the ToCSV() Method
        public String DepartureDateStr
        {
            get
            {
                String dateFormat = "dd-MM-yyyy";
                return this.DepartureDate.ToString(dateFormat);
            }
        }
        // Total cost method, returns the sum of the room cost for the total nights and the cost of the extras
        public double GetCost()
        {   
            return GetExtrasCost() + (GetRoomCost() * this.Nights);
        }
        // Returns just the cost of the room per night for all guests
        public double GetRoomCost()
        {
            double cost = 0;
            foreach (Guest g in this.guests)
            {
                int nightCost = g.Age < 18 ? 30 : 50;
                cost += nightCost;
            }
            return cost;
        }
        // Returns just the extras cost
        public double GetExtrasCost()
        {
            double cost = 0;
            foreach (Extra e in this.extras.Values)
            {
                cost += e.GetCost();
            }
            return cost;
        }
        // Returns a string erpresentation of this object for CSV persistance. Uses the CSVUtil class for its formatting methods
        public String ToCSV()
        {
            String sep = CSVUtil.Separator.ToString();
            // Returns the values needed to store separated by a tab
            string[] props = new string[] {
                CSVUtil.Format(this.bookingId.ToString()),
                CSVUtil.Format(this.cust.CustomerId.ToString()),
                CSVUtil.Format(this.ArrivalDateStr),
                CSVUtil.Format(this.DepartureDateStr),
                CSVUtil.Format(String.Join(",",this.Guests.Where(item => item != null).Select(item => item.Passport)))
            };
            return String.Join(sep, props);

        }
    }
}
