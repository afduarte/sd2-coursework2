﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidaySystem
{
    class CarHire : Extra
    {
        /*
         * Antero Ferreira Duarte - 40211946
         * CarHire Class - Inherits From Extra(abstract) - Relates to Booking (belongs to)
         * Creates a new car hire extra validating input values, 
         * overrides GetCost() from Extra and ToCSV() originally from CSVWritable (through Extra)
         * Last Modified: 05-12-2016
         */
        // Keeps a reference to the booking
        private Booking booking;
        // Private variable to store the driver name
        private String driver;
        // Public accessor methods for the driver name, perform validation
        public String Driver
        {
            get
            {
                return this.driver;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || String.IsNullOrEmpty(value))
                {
                    throw new Exception("The Driver Name must not be empty");
                }
                this.driver = value;
            }
        }
        // Private variable to store the start date for the car hire
        private DateTime start;
        // Public accessor methods for the start date, perform validation
        public DateTime StartDate
        {
            get
            {
                return this.start;
            }
            set
            {
                if (value == DateTime.MinValue)
                {
                    throw new Exception("Invalid date: Start");
                }
                this.start = value;
            }
        }
        // Private variable to store the end date for the car hire
        private DateTime end;
        // Public accessor methods for the end date, perform validation
        public DateTime EndDate
        {
            get
            {
                return this.end;
            }
            set
            {
                if (value == DateTime.MinValue)
                {
                    throw new Exception("Invalid date: End");
                }
                this.end = value;
            }
        }
        // Creates a new CarHire object with dates as DateTime objects
        public CarHire(Booking b, DateTime start, DateTime end, String driver)
        {
            this.booking = b;
            if(start < this.booking.ArrivalDate || end > this.booking.DepartureDate)
            {
                throw new Exception("A car can only be hired within the range of the booking");
            }
            this.StartDate = start;
            this.EndDate = end;
            this.Driver = driver;
        }
        // Creates a new CarHire object with dates as strings
        public CarHire(Booking b, String startStr, String endStr, String driver)
            : this(b, Convert.ToDateTime(startStr), Convert.ToDateTime(endStr),driver){}
        // Override of the GetCost() method in the abstract parent class that returns the total cost of the car hire for this booking
        public override double GetCost()
        {
            // Return the cost per day (add 1 to the Days to get the number of different dates, not just the number of days in between)
            return  50 * ((this.end - this.start).Days + 1);
        }
        // Returns a string representation of this object for CSV persistance. Uses the CSVUtil class for its formatting methods
        public override string ToCSV()
        {
            String dateFormat = "dd-MM-yyyy";

            String sep = CSVUtil.Separator.ToString();
            // Returns the values needed to store separated by a tab
            string[] props = new string[] {
                CSVUtil.Format(this.booking.BookingId.ToString()),
                CSVUtil.Format(this.start.ToString(dateFormat)),
                CSVUtil.Format(this.end.ToString(dateFormat)),
                CSVUtil.Format(this.driver)
            };
            return String.Join(sep, props);
        }
    }
}
