﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HolidaySystem
{
    public partial class ShowCustomersWindow : Window
    {
      /*
       * Antero Ferreira Duarte - 40211946
       * BookingWindow Class
       * Handles shows a list of customers, allows to edit existing, remove or add new customers
       * Last Modified: 05-12-2016
       */
        public ShowCustomersWindow()
        {
            InitializeComponent();
            RefreshList();
            this.Activated += ActivatedHandler;
        }
        private void btn_deleteCustomer_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            Customer clicked = (Customer)button.CommandParameter;
            try
            {
                StoreFacade.DeleteCustomer(clicked);
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            finally
            {
                RefreshList();
            }
        }
        private void btn_editCustomer_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            Customer clicked = (Customer)button.CommandParameter;
            CustomerWindow w = new CustomerWindow(clicked);
            // Set the Owner and position of the new window
            w.Owner = this;
            w.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            w.ShowDialog();
        }
        private void btn_addCustomer_Click(object sender, RoutedEventArgs e)
        {
            CustomerWindow w = new CustomerWindow();
            // Set the Owner and position of the new window
            w.Owner = this;
            w.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            w.ShowDialog();
        }
        private void ActivatedHandler(object sender, EventArgs e)
        {
            RefreshList();
        }
        private void RefreshList()
        {
            lview_customers.ItemsSource = StoreFacade.GetCustomers();
            
            foreach (var col in gview_customers.Columns)
            {
                if (double.IsNaN(col.Width)) col.Width = col.ActualWidth;
                col.Width = double.NaN;
            }
        }
    }
}
