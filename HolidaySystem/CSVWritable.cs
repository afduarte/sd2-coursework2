﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidaySystem
{
    public interface CSVWritable
    {
        /*
         * Antero Ferreira Duarte - 40211946
         * CSVWritable Interface
         * Provides a contract for entities prone to be persisted in the CSV format
         * Last Modified: 17-11-2016
         */
        // Method that returns a string in the CSV Format for saving to the file.
        // Generating the right format is the responsability of every class that implements this method.
        String ToCSV();
    }
}
