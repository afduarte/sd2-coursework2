﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HolidaySystem
{
        /*
       * Antero Ferreira Duarte - 40211946
       * BookingWindow Class
       * Handles showing bookings including the invoice
       * Last Modified: 05-12-2016
       */
    public partial class ShowBookingWindow : Window
    {
        public ShowBookingWindow(Booking b)
        {
            InitializeComponent();
            lbl_bookingId.Content = b.BookingId;
            lbl_arrivalDate.Content = b.ArrivalDateStr;
            lbl_departureDate.Content = b.DepartureDateStr;
            lbl_custInfo.Content = b.Customer.CustomerId + "\t" + b.Customer.Name + "\t" + b.Customer.Address;
            lview_guests.ItemsSource = b.Guests;
            lbl_nightCost.Content = b.Nights + " x £" + b.GetRoomCost();
            lbl_extrasCost.Content = "£" + b.GetExtrasCost();
            lbl_bookingCost.Content = "£"+ b.GetCost();

        }
    }
}
