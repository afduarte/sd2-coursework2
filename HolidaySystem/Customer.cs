﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidaySystem
{
    public class Customer : Person, CSVWritable
    {
        /*
         * Antero Ferreira Duarte - 40211946
         * Customer Class - Implements CSVWritable(interface), Inherits from Person
         * Creates a new customer object validating input values
         * Provides access to its private attributes through {get; set;} methods
         * Last Modified: 26-11-2016
         */
        // Private variable to store the customer address
        private String address;
        // Public accessor for the customer address
        public String Address
        {
            get
            {
                return this.address;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || String.IsNullOrEmpty(value))
                {
                    throw new Exception("The address can not be empty");
                }
                this.address = value.Replace('\n', ' ');
            }
        }
        // Private variable to store the customer ID
        private int custId;
        // Public accessor for the customer ID, the set method is blocked
        public int CustomerId {
            get
            {
                return this.custId;
            }
            set
            {
                this.custId = value;
            } 
        }
        // Customer constructor that takes an id parameter for when reading from the file
        public Customer(int id, String name, String address)
            : base(name)
        {
            this.Address = address;
            this.CustomerId = id;
        }
        // Customer constructor that connects to the store to generate an ID
        public Customer(String name, String address)
            : this(StoreFacade.GenerateCustomerId(),name,address) {}
        // Implement the ToCSV Method
        public String ToCSV()
        {
            String sep = CSVUtil.Separator.ToString();
            // Returns the values needed to store separated by a tab
            string[] props = new string[] {
                CSVUtil.Format(this.CustomerId.ToString()),
                CSVUtil.Format(this.Name),
                CSVUtil.Format(this.Address)
            };
            return String.Join(sep, props);
        }
    }
}
