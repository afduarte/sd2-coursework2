﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HolidaySystem
{
    public partial class BookingWindow : Window
    {
        /*
        * Antero Ferreira Duarte - 40211946
        * BookingWindow Class
        * Handles creating and changing bookings
        * Last Modified: 05-12-2016
        */
        private List<Customer> Customers 
        {
            get
            {
                return new List<Customer>(StoreFacade.GetCustomers());
            }
        }
        private List<Guest> Guests
        {
            get
            {
                List<Guest> list = new List<Guest>();
                list.Add(null);
                list.AddRange(StoreFacade.GetGuests());
                return list;
            }
        }
        private Brush red;
        private Brush green;
        private Booking book;
        // Constructor for create booking window
        public BookingWindow()
        {
            InitializeComponent();
            this.book = null;
            cmb_customer.ItemsSource = Customers;
            cmb_customer.SelectedIndex = 0;
            cmb_guests.ItemsSource = Guests;
            cmb_guests.SelectedIndex = 0;
            BrushConverter bc = new BrushConverter();
            this.green = (Brush)bc.ConvertFrom("#3063ff92");
            this.red = (Brush)bc.ConvertFrom("#30ff6363");
            HandleExtraButtons();
            this.Activated += this.HandleGuests;
        }
        // Constructor for amend booking window
        public BookingWindow(Booking b)
        {
            InitializeComponent();
            this.book = b;
            List<Customer> custs = Customers;
            cmb_customer.ItemsSource = custs;
            cmb_customer.SelectedIndex = custs.IndexOf(b.Customer);
            lbl_bookingId.Content = b.BookingId;
            dtp_arrivalDate.SelectedDate = b.ArrivalDate;
            dtp_departureDate.SelectedDate = b.DepartureDate;
            cmb_guests.ItemsSource = Guests;
            BrushConverter bc = new BrushConverter();
            this.green = (Brush)bc.ConvertFrom("#3063ff92");
            this.red = (Brush)bc.ConvertFrom("#30ff6363");
            HandleExtraButtons();
            RefreshGuests();
            this.Activated += this.HandleGuests;
        }

        private void HandleExtraButtons()
        {
            btn_breakfast.Background = this.book != null && this.book.Extras.ContainsKey("Breakfast") ? this.green : this.red;
            btn_car_hire.Background = this.book != null && this.book.Extras.ContainsKey("CarHire") ? this.green : this.red;
            btn_dinner.Background = this.book != null && this.book.Extras.ContainsKey("Dinner") ? this.green : this.red;
        }

        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            Booking b = this.book;
            try
            {
                DateTime arrival = dtp_arrivalDate.SelectedDate.GetValueOrDefault(DateTime.MinValue);
                DateTime departure = dtp_departureDate.SelectedDate.GetValueOrDefault(DateTime.MinValue);
                Customer cust = (Customer)cmb_customer.SelectedValue;
                if (b == null)
                {
                    b = new Booking(cust, arrival, departure);
                    this.book = b;
                    StoreFacade.CreateBooking((Booking)this.book);

                }
                else
                {
                    b.ArrivalDate = arrival;
                    b.DepartureDate = departure;
                    b.Customer = cust;
                }
                MessageBox.Show("Booking saved successfully");
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void btn_removeGuest_Click(object sender, RoutedEventArgs e)
        {
            Booking b = (Booking)this.book;
            Button button = (Button)sender;
            Guest clicked = (Guest)button.CommandParameter;
            b.RemoveGuest(clicked);
            RefreshGuests();
        }
        private void btn_addGuest_Click(object sender, RoutedEventArgs e)
        {
            Booking b = this.book;
            
            if (b == null)
            {
                MessageBox.Show("When creating a new booking, you must save the booking values before adding guests, please click save");
                return;
            }
            if (cmb_guests.SelectedValue == null)
            {
                GuestWindow w = new GuestWindow();
                w.Owner = this;
                w.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                w.ShowDialog();
                return;
            }
            try
            {
                Guest selected = (Guest)cmb_guests.SelectedValue;
                b.AddGuest(selected);
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            RefreshGuests();
        }

        private void HandleGuests(object sender, EventArgs e)
        {
            this.RefreshGuests();
        }
        private void RefreshGuests()
        {

            Booking b = this.book;
            cmb_guests.ItemsSource = this.Guests;
            if (b != null)
            {
                lview_guests.ItemsSource = new List<Guest>(b.Guests);
            }
        }

        private void btn_breakfast_Click(object sender, RoutedEventArgs e)
        {
            if(this.book == null)
            {
                MessageBox.Show("You must save the booking you're creating before you can add extras to it.");
                return;
            }
            grd_breakfast.Visibility = Visibility.Visible;
            grd_dinner.Visibility = Visibility.Hidden;
            grd_car_hire.Visibility = Visibility.Hidden;
            Extra extra;
            if (this.book.Extras.TryGetValue(typeof(HolidaySystem.Breakfast).Name, out extra))
            {
                Breakfast b = (Breakfast)extra;
                txt_breakfast_desc.Text = b.Description;
                btn_remove_breakfast.Visibility = Visibility.Visible;
            }
        }
        private void btn_dinner_Click(object sender, RoutedEventArgs e)
        {
            if (this.book == null)
            {
                MessageBox.Show("You must save the booking you're creating before you can add extras to it.");
                return;
            }
            grd_breakfast.Visibility = Visibility.Hidden;
            grd_dinner.Visibility = Visibility.Visible;
            grd_car_hire.Visibility = Visibility.Hidden;
            Extra extra;
            if (this.book.Extras.TryGetValue(typeof(HolidaySystem.Dinner).Name, out extra))
            {
                Dinner d = (Dinner)extra;
                txt_dinner_desc.Text = d.Description;
                btn_remove_dinner.Visibility = Visibility.Visible;
            }
        }
        private void btn_car_hire_Click(object sender, RoutedEventArgs e)
        {
            if (this.book == null)
            {
                MessageBox.Show("You must save the booking you're creating before you can add extras to it.");
                return;
            }
            grd_breakfast.Visibility = Visibility.Hidden;
            grd_dinner.Visibility = Visibility.Hidden;
            grd_car_hire.Visibility = Visibility.Visible;
            Extra extra;
            if (this.book.Extras.TryGetValue(typeof(HolidaySystem.CarHire).Name, out extra))
            {
                CarHire c = (CarHire)extra;
                txt_driver.Text = c.Driver;
                dtp_car_start.SelectedDate = c.StartDate;
                dtp_car_end.SelectedDate = c.EndDate;
                btn_remove_car_hire.Visibility = Visibility.Visible;
            }
        }

        private void btn_save_breakfast_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.book.AddExtra(new Breakfast(this.book, txt_breakfast_desc.Text));
                HandleExtraButtons();
                btn_remove_breakfast.Visibility = Visibility.Visible;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
            
        }
        private void btn_remove_breakfast_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.book.RemoveExtra(typeof(HolidaySystem.Breakfast).Name);
                HandleExtraButtons();
                btn_remove_breakfast.Visibility = Visibility.Hidden;
                txt_breakfast_desc.Text = String.Empty;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void btn_save_dinner_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.book.AddExtra(new Dinner(this.book, txt_dinner_desc.Text));
                HandleExtraButtons();
                btn_remove_dinner.Visibility = Visibility.Visible;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
        private void btn_remove_dinner_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.book.RemoveExtra(typeof(HolidaySystem.Dinner).Name);
                HandleExtraButtons();
                btn_remove_dinner.Visibility = Visibility.Hidden;
                txt_dinner_desc.Text = String.Empty;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void btn_save_car_hire_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.book.AddExtra(new CarHire(this.book, dtp_car_end.SelectedDate.GetValueOrDefault(DateTime.MinValue), dtp_car_end.SelectedDate.GetValueOrDefault(DateTime.MinValue), txt_driver.Text));
                HandleExtraButtons();
                btn_remove_car_hire.Visibility = Visibility.Visible;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
        private void btn_remove_car_hire_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.book.RemoveExtra(typeof(HolidaySystem.CarHire).Name);
                HandleExtraButtons();
                btn_remove_car_hire.Visibility = Visibility.Hidden;
                txt_driver.Text = String.Empty;
                dtp_car_start.SelectedDate = DateTime.Today;
                dtp_car_end.SelectedDate = DateTime.Today;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

    }
}