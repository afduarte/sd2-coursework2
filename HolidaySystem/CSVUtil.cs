﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HolidaySystem
{
    public class CSVUtil
    {
        /*
         * Antero Ferreira Duarte - 40211946
         * CSVUtil Class - Relates to [1..1] CSVUtil (as per the specification for the Singleton Pattern)
         * Util class Implemented as a Singleton to write to the CSV Files
         * Also provides some util static methods to convert to and from the csv format
         * Last Modified: 04-12-2016
         */

        // Store a reference to the files to use as FileStreams (can be used for both reading and writing)
        private FileStream customerFile;
        private FileStream bookingFile;
        private FileStream guestsFile;
        private FileStream mealsFile;
        private FileStream carHireFile;
        // Static variable that represents the character that separates fields on every object turned into CSV Format
        public static readonly char Separator = '\t';
        // Make the class have a reference to an instance of it's own type, thereby implementing the Singleton pattern
        private static CSVUtil instance;
        // Public acessor methods for the instance variable
        public static CSVUtil Instance
        {
            get
            {
                // return the existing instance or create a new one and store it accordingly
                if (instance == null)
                {
                    instance = new CSVUtil();
                    instance.InitializeFiles();
                }
                return instance;
            }
        }
        // Override the constructor to prevent code from creating more than one instance of the class
        private CSVUtil() {}
        // Set the references to the files used for persistence
        private void InitializeFiles()
        {
            string path = Directory.GetCurrentDirectory();
            this.customerFile = new FileStream(path + "\\customers.csv", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            this.bookingFile = new FileStream(path + "\\bookings.csv", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            this.guestsFile = new FileStream(path + "\\guests.csv", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            this.carHireFile = new FileStream(path + "\\car-hires.csv", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            this.mealsFile = new FileStream(path + "\\meals.csv", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
        }
        // Clear the files, method used before saving
        public void ClearFiles()
        {
            this.bookingFile.SetLength(0);
            this.bookingFile.Close();
            this.customerFile.SetLength(0);
            this.customerFile.Close();
            this.guestsFile.SetLength(0);
            this.guestsFile.Close();
            this.carHireFile.SetLength(0);
            this.carHireFile.Close();
            this.mealsFile.SetLength(0);
            this.mealsFile.Close();
            this.InitializeFiles();

        }
        // Append any object that implements the CSVWritable interface to the right file
        public void AppendToFile(CSVWritable input)
        {
            StreamWriter sw = new StreamWriter(GetFileFromType(input.GetType()));
            sw.AutoFlush = true;
            sw.WriteLine(input.ToCSV());
        }
        // Read one type of object from its respectve file
        public List<String[]> ReadFromFile(Type t)
        {
            StreamReader reader = new StreamReader(GetFileFromType(t));
            List<String[]> lines = new List<String[]>();

            while (!reader.EndOfStream)
            {
                lines.Add(ToArray(reader.ReadLine()));
            }
            return lines;
        }
        // Returns the right file for the type of object passed in
        private FileStream GetFileFromType(Type t)
        {
            if (t.Equals(typeof(HolidaySystem.Customer)))
            {
                return this.customerFile;
            }
            else if (t.Equals(typeof(HolidaySystem.Booking)))
            {
                return this.bookingFile;
            }
            else if (t.Equals(typeof(HolidaySystem.CarHire)))
            {
                return this.carHireFile;
            }
            else if (t.Equals(typeof(HolidaySystem.Breakfast)) || t.Equals(typeof(HolidaySystem.Dinner)))
            {
                return this.mealsFile;
            }
            else if (t.Equals(typeof(HolidaySystem.Guest)))
            {
                return this.guestsFile;
            }
            else
            {
                throw new Exception("Cannot save object, there's no file assigned to type: " + t.FullName);
            }
        }
        // Util methods to escape, unescape and convert a CSV formatted String to an array of strings
        public static String Unquote(String value)
        {
            String unquoted = value;
            if (value[0] == '"')
            {
                unquoted = unquoted.Substring(1);
            }
            if (value[value.Length - 1] == '"')
            {
                unquoted = unquoted.Remove(unquoted.Length - 1);
            }
            return unquoted;
        }
        public static String Format(String value)
        {
            return String.Format("\"{0}\"", value.Replace("\"", "'").Replace("\t", " "));
        }
        public static String[] ToArray(String value)
        {
            String[] elements = value.Split(Separator);

            for (int i = 0; i < elements.Length; i++)
            {
                elements[i] = CSVUtil.Unquote(elements[i]);
            }
            return elements;
        }
    }
}
