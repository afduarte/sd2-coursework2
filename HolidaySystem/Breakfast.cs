﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidaySystem
{
    public class Breakfast : Extra
    {
        /*
         * Antero Ferreira Duarte - 40211946
         * Dinner Class - Inherits From Extra(abstract) - Relates to Booking (belongs to)
         * Creates a new breakfast extra validating input values, 
         * overrides GetCost() from Extra and ToCSV() originally from CSVWritable (through Extra)
         * Last Modified: 05-12-2016
         */
        // Keep a reference to the booking this extra belongs to
        private Booking booking;
        // Private String to store the dietary requirements
        private String description;
        // Public accessor for the description property
        public String Description
        {
            get
            {
                return this.description;
            }
            set
            {
                this.description = value;
            }
        }
        // Constructor for the Breakfast object, stores the reference to the booking and the description string
        public Breakfast(Booking b,String description)
        {
            this.booking = b;
            this.Description = description;

        }
        // Override of the GetCost() method in the abstract parent class that returns the total cost of breakfast for this booking
        public override double GetCost()
        {
            // Return the cost per day, assuming a guest doesn't have breakfast on the day they check in
            return  5 * this.booking.Guests.Count * (this.booking.Nights);
        }
        // Override of the ToCSV() method in the CSVWritable interface (through the parent abstract class)
        public override string ToCSV()
        {
            String sep = CSVUtil.Separator.ToString();
            // Returns the values needed to store separated by a tab
            string[] props = new string[] {
                CSVUtil.Format(this.booking.BookingId.ToString()),
                CSVUtil.Format(this.description),
                CSVUtil.Format(this.GetType().Name)
            };
            return String.Join(sep, props);
        }
    }
}
