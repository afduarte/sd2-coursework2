﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HolidaySystem
{
    /// <summary>
    /// Interaction logic for GuestWindow.xaml
    /// </summary>
    public partial class GuestWindow : Window
    {
        private Guest guest;
        public GuestWindow()
        {
            InitializeComponent();
            this.guest = null;
        }
        public GuestWindow(Guest g)
        {
            InitializeComponent();
            this.guest = g;
            txt_Name.Text = this.guest.Name;
            txt_Age.Text = this.guest.Age.ToString();
        }

        private void btn_Add_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.guest == null)
                {
                    try
                    {
                        int guestAge = Int32.Parse(txt_Age.Text);
                        this.guest = new Guest(txt_Passport.Text, txt_Name.Text, guestAge);
                        StoreFacade.AddGuest(this.guest);
                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show(exp.Message);
                    }
                }
                else
                {
                    this.guest.Name = txt_Name.Text;
                    try
                    {
                        this.guest.Age = Int32.Parse(txt_Age.Text);
                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show(exp.Message);
                    }
                }
                
                this.Close();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
    }
}
