﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.Generic;

namespace HolidaySystem
{
    public partial class MainWindow : Window
    {
        /*
        * Antero Ferreira Duarte - 40211946
        * MainWindow Class
        * Handles all the main functionality, including creating other windows
        * Last Modified: 05-12-2016
        */
        private bool ShowActive = true;
        private bool ShowPast = false;
        private bool ShowFuture = true;

        public MainWindow()
        {
            StoreFacade.Load();
            InitializeComponent();
            lview_bookings.ItemsSource = this.FilterBookings(ShowActive, ShowFuture, ShowPast);
            this.Activated += HandleVisibleBookings;

        }

        private List<Booking> FilterBookings(bool active, bool future, bool past)
        {
            List<Booking> unfiltered = StoreFacade.GetBookings();

            if (!active)
            {
                unfiltered.RemoveAll(item => {
                    return DateTime.Today > item.ArrivalDate && DateTime.Today < item.DepartureDate;
                });
            }
            if (!future)
            {
                unfiltered.RemoveAll(item =>
                {
                    return item.ArrivalDate > DateTime.Today;
                });
            }
            if (!past)
            {
                unfiltered.RemoveAll(item =>
                {
                    return item.DepartureDate < DateTime.Today;
                });
            }
            // return as a binding list
            return new List<Booking>(unfiltered);

        }
        private void btn_showBooking_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            Booking clicked = (Booking)button.CommandParameter;
            ShowBookingWindow w = new ShowBookingWindow(clicked);
            // Set the Owner and position of the new window
            w.Owner = this;
            w.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            w.ShowDialog();
        }
        private void btn_amendBooking_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            Booking clicked = (Booking)button.CommandParameter;
            BookingWindow w = new BookingWindow(clicked);
            w.Owner = this;
            w.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            w.ShowDialog();
        }
        private void btn_addBooking_Click(object sender, RoutedEventArgs e)
        {
            BookingWindow w = new BookingWindow();
            w.Owner = this;
            w.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            w.ShowDialog();
        }
        private void btn_manageCustomers_Click(object sender, RoutedEventArgs e)
        {
            ShowCustomersWindow w = new ShowCustomersWindow();
            w.Owner = this;
            w.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            w.ShowDialog();
        }

        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            // Store everything on save
            StoreFacade.Save();
        }

        public void HandleVisibleBookings(object sender, EventArgs e)
        {
            this.ShowActive = (bool)chk_active.IsChecked;
            this.ShowPast = (bool)chk_past.IsChecked;
            this.ShowFuture = (bool)chk_future.IsChecked;
            lview_bookings.ItemsSource = this.FilterBookings(ShowActive, ShowFuture, ShowPast);
        }

        private void btn_deleteBooking_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button button = (Button)sender;
                Booking clicked = (Booking)button.CommandParameter;
                StoreFacade.DeleteBooking(clicked);
                lview_bookings.ItemsSource = this.FilterBookings(this.ShowActive, this.ShowFuture, this.ShowPast);
                MessageBox.Show("Booking deleted successfully!");
            }
            catch (Exception exp)
            {
                MessageBox.Show("Error when deleting the booking");
            }
            

        }
    }
}
