﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidaySystem
{
    public class Person
    {
        /*
         * Antero Ferreira Duarte - 40211946
         * Person Class
         * Provides access to its private attributes through {get; set;} methods
         * Last Modified: 05-12-2016
         */
        // Keep a private variable for the name
        private String name;
        // Public accessor methods for the name
        public String Name 
        {
            get 
            {
                return this.name;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || String.IsNullOrWhiteSpace(value))
                {
                    throw new Exception("The Name can not be empty");
                }
                this.name = value;
            } 
        }
        // Constructor that sets the name
        public Person(String name)
        {
            this.Name = name;
        }
    }
}
