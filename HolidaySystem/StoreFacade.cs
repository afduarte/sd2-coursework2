﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidaySystem
{
    public class StoreFacade
    {
        /*
         * Antero Ferreira Duarte - 40211946
         * StoreFacade Class - Relates to [1..1] Store
         * Implemented as a Facade Class, hides the complexity in the store
         * Some of the methods here only call one method in the store, but having them all here 
         * means someone writing the GUI would never need to see the store class. 
         * Also, the complexity and the fact that order matters in some of the 
         * methods in the store makes this class worth having.
         * Provides methods from the store class enforcing the right order.
         * Implemented as a static class (in the sense that it only has static methods)
         * although not formally (the class declaration doesn't state static)
         * Last Modified: 01-12-2016
         */
        // Keep a reference to the store singleton
        private static Store store = Store.Instance;
        // Returns the list of bookings from the store
        public static List<Booking> GetBookings()
        {
            return store.GetBookings();
        }
        // Returns the list of guests from the store
        public static List<Guest> GetGuests()
        {
            return store.GetGuests();
        }
        // Returns the list of Customers from the store
        public static List<Customer> GetCustomers()
        {
            return store.GetCustomers();
        }
        // Simply chains the delete call into the store
        public static void DeleteCustomer(Customer cust)
        {
            store.DeleteCustomer(cust);
        }
        // Gets a new CustomerId from the store
        public static int GenerateCustomerId()
        {
            return store.GenCustomerId();
        }
        // Simply chains the add call into the store
        public static void AddCustomer(Customer c)
        {
            store.AddCustomer(c);
        }
        // Simply chains the delete call into the store
        public static void DeleteBooking(Booking b)
        {
            store.DeleteBooking(b);
        }
        // Gets a new BookingId from the store
        public static int GenerateBookingId()
        {
            return store.GenBookingId();
        }
        // Abstracts all the steps of creating a booking in the store
        public static void CreateBooking(Booking b)
        {
            store.AddBooking(b);
            store.AddCustomer(b.Customer);
            foreach (Guest g in b.Guests)
            {
                store.AddGuest(g);
            }
        }
        // Simply chains the add call into the store
        public static void AddGuest(Guest g)
        {
            store.AddGuest(g);
        }
        // Abstracts the fact that the files have to be cleared before saving
        // and provides a single method to save every type of CSVWritable
        public static void Save()
        {
            store.ClearFiles();
            store.SaveCustomers();
            store.SaveBookings();
            store.SaveGuests();
        }
        // Abstracts the fact that the files have to be loaded in a specific order 
        // and provides a single method to load them all
        public static void Load()
        {
            store.LoadCustomers();
            store.LoadGuests();
            store.LoadBookings();
            store.LoadExtras();
        }
    }
}
